﻿using System;
using System.Collections.Generic;

namespace DefinitelyNotNotebook
{
    class Notebook
    {
        public static Dictionary<string, Note> notebook = new Dictionary<string, Note>();
        static void Main(string[] args)
        {
            Console.WriteLine("Привет! Я твоя телевонная книжка.");
            Console.WriteLine("Что ты хочешь сделать???");
            Console.WriteLine("Введите ДОБАВИТЬ, если хотите добавить новую запись");
            Console.WriteLine("Введите РЕДАКТИРОВАТЬ, ИМЯ и ФАМИЛИЮ пользователя, если хотите изменить старую запись(Напиример, РЕДАКТИРОВАТЬ Остап Бендер)");
            Console.WriteLine("Введите УДАЛИТЬ, ИМЯ и ФАМИЛИЮ пользователя, если хотите удалить старую запись(Напиример, УДАЛИТЬ Энакин Скайуокер)");
            Console.WriteLine("Введите НАЙТИ, ИМЯ и ФАМИЛИЮ пользователя, если хотите увидеть старую запись(Напиример, ПРОСМОТР Энтони Старк)");
            Console.WriteLine("Введите ПРОСМОТР, если хотите увидеть все записи");
            Console.WriteLine("Введите ВЫХОД, если хотите выйти из телефонной книжки");
            Console.WriteLine("Введите команду");
            string[] zapros = Console.ReadLine().ToUpper().Split(' ');
            bool work = true;
            while (work)
            {
                switch (zapros[0])
                {
                    case "ДОБАВИТЬ":
                        try
                        {
                            Note note = new Note();
                            note.Add();
                            notebook.Add(note.Name.ToUpper() + note.Surname.ToUpper(), note);
                            Console.WriteLine("Добавлена запись");
                            Console.WriteLine("============================================================");
                            Console.WriteLine(note.ToString());
                            Console.WriteLine("============================================================");
                            Console.WriteLine("Телефонная книжка растет)");
                            break;
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("Такой пользователь уже есть");
                            break;
                        }

                    case "РЕДАКТИРОВАТЬ":
                        if (zapros.Length == 3)
                        {
                            string zap2 = zapros[1] + zapros[2];
                            int zapCount2 = 0;
                            foreach (KeyValuePair<string, Note> item in notebook)
                            {
                                if (zap2 == item.Key)
                                {
                                    notebook.Remove(item.Key);
                                    zapCount2 += 1;
                                    Note note1 = new Note();
                                    note1.Add();
                                    notebook.Add(note1.Name.ToUpper() + note1.Surname.ToUpper(), note1);
                                    Console.WriteLine("Добавлена запись");
                                    Console.WriteLine(note1.ToString());
                                    Console.WriteLine("Телефонная книжка растет)");
                                    break;
                                }
                            }
                            if (zapCount2 == 0)
                            {
                                Console.WriteLine("Извините, запись с таким именем не найдена.");
                            }
                            break;
                        }
                        else
                        {
                            Console.WriteLine("Неправильно составлен запрос");
                            break;
                        }
                    case "УДАЛИТЬ":
                        if (zapros.Length == 3)
                        {
                            string zap1 = zapros[1] + zapros[2];
                            int zapCount1 = 0;
                            foreach (KeyValuePair<string, Note> item in notebook)
                            {
                                if (zap1 == item.Key)
                                {
                                    notebook.Remove(item.Key);
                                    zapCount1 += 1;
                                    break;
                                }
                            }
                            if (zapCount1 == 0)
                            {
                                Console.WriteLine("Извините, запись с таким именем не найдена.");
                            }
                            break;
                        }
                        else
                        {
                            Console.WriteLine("Неправильно составлен запрос");
                            break;
                        }
                    case "НАЙТИ":
                        if (zapros.Length == 3)
                        {
                            string zap = zapros[1] + zapros[2];
                            int zapCount = 0;
                            foreach (KeyValuePair<string, Note> item in notebook)
                            {
                                if (zap == item.Key)
                                {
                                    Console.WriteLine("==============================================================");
                                    Console.WriteLine(item.Value.ToString());
                                    Console.WriteLine("==============================================================");
                                    zapCount += 1;
                                }
                            }
                            if (zapCount == 0)
                            {
                                Console.WriteLine("Извините, запись с таким именем не найдена.");
                            }
                            break;
                        }
                        else
                        {
                            Console.WriteLine("Неправильно составлен запрос");
                            break;
                        }
                    case "ПРОСМОТР":
                        foreach (KeyValuePair<string, Note> item in notebook)
                        {
                            Console.WriteLine("==============================================================");
                            Console.WriteLine(item.Value.Surname);
                            Console.WriteLine(item.Value.Name);
                            Console.WriteLine(item.Value.Number);
                            Console.WriteLine("==============================================================");
                        }
                        break;
                    case "ВЫХОД":
                        work = false;
                        Console.WriteLine("До скорой встречи, До скорой встречи, Моя работа закончена навечно");
                        break;
                    default:
                        Console.WriteLine("Что-то где-то когда-то пошло не так. Команда не найдена. Повторите ввод.");
                        break;
                }
                if (work)
                {
                    Console.WriteLine("Введите команду");
                    zapros = Console.ReadLine().ToUpper().Split(' ');
                }
            }
        }
    }
    public class Note
    {
        public string Surname { get; set; }
        public string Name { get; set; }
        public string DadName { get; set; }
        public long Number { get; set; }
        public string Country { get; set; }
        public DateTime DateBirth { get; set; }
        public string Organization { get; set; }
        public string Position { get; set; }
        public string Other { get; set; }
        public void Add()
        {
            Console.WriteLine("Введите Имя");
            while (true)
            {
                string popytka = Console.ReadLine();
                if (popytka == "")
                {
                    Console.WriteLine("Имя не может быть пустым. Введите ещё раз.");
                }
                else
                {
                    this.Name = popytka;
                    break;
                }
            }
            Console.WriteLine("Введите Фамилию");
            while (true)
            {
                string popytka = Console.ReadLine();
                if (popytka == "")
                {
                    Console.WriteLine("Фамилия не может быть пустой. Введите ещё раз.");
                }
                else
                {
                    this.Surname = popytka;
                    break;
                }
            }
            Console.WriteLine("Введите Отчество(является необязательным, чтобы пропустить нажмите Enter)");
            string dad = Console.ReadLine();
            if (dad == "")
            {
                DadName = "-----";
            }
            else
            {
                DadName = dad;
            }
            Console.WriteLine("Введите Номер Телефона(Телефонный номер состоит из 11 цифр написанных подряд без разделения)");
            while (true)
            {
                string popytka = Console.ReadLine();
                if (popytka == "")
                {
                    Console.WriteLine("Номер телефона не может быть пустым. Введите ещё раз.");
                }
                else if (popytka.Length != 11)
                {
                    Console.WriteLine("Неверное количество символов. Введите ещё раз.");
                }
                else if (popytka.Length == 11)
                {
                    char[] ch = popytka.ToCharArray();
                    int count = 0;
                    for (int i = 0; i < 11; i++)
                    {
                        string z = "";
                        z = z + ch[i];
                        if (!int.TryParse(z, out int tpn))
                        {
                            count += 1;
                        }
                        z = "";
                    }
                    if (count == 0)
                    {
                        this.Number = Convert.ToInt64(popytka);
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Неверный формат. Введите ещё раз.");
                    }
                }
            }
            Console.WriteLine("Введите Название страны");
            while (true)
            {
                string popytka = Console.ReadLine();
                if (popytka == "")
                {
                    Console.WriteLine("Название страны не может быть пустым. Введите ещё раз.");
                }
                else
                {
                    this.Country = popytka;
                    break;
                }
            }
            while (true)
            {
                Console.WriteLine("Введите День рождения в формате День.НомерМесяца.Год (является необязательным, чтобы пропустить нажмите Enter)");
                try
                {
                    while (true)
                    {
                        string popytkadate = Console.ReadLine();
                        if (popytkadate != "")
                        {
                            string[] date = popytkadate.Split('.');
                            if (date.Length == 3)
                            {


                                if (int.TryParse(date[0], out int tpd1) && int.TryParse(date[2], out int tpd2) && int.TryParse(date[1], out int tpd3))
                                {
                                    this.DateBirth = new DateTime(Convert.ToInt32(date[2]), Convert.ToInt32(date[1]), Convert.ToInt32(date[0]));
                                    break;
                                }
                                else
                                {
                                    Console.WriteLine("Неверный формат. Повторите Ввод.");
                                }
                            }
                            else
                            {
                                Console.WriteLine("Неверный формат. Повторите Ввод.");
                            }
                        }
                        else
                        {
                            this.DateBirth = DateTime.MinValue;
                            break;
                        }
                    }
                    break;
                }
                catch (Exception e)
                {
                    Console.WriteLine("Вы что-то напутали в дате.");
                }
            }
            Console.WriteLine("Введите Организацию(является необязательным, чтобы пропустить нажмите Enter)");
            string org = Console.ReadLine();
            if (org == "")
            {
                Organization = "-----";
            }
            else
            {
                Organization = org;
            }
            Console.WriteLine("Введите Должность(является необязательным, чтобы пропустить нажмите Enter)");
            string pos = Console.ReadLine();
            if (pos == "")
            {
                Position = "-----";
            }
            else
            {
                Position = pos;
            }
            Console.WriteLine("Введите Дополнительную Информацию(является необязательным, чтобы пропустить нажмите Enter)");
            string oth = Console.ReadLine();
            if (oth == "")
            {
                Other = "-----";
            }
            else
            {
                Other = oth;
            }
        }
        public override string ToString()
        {
            return $"Имя:{Name}\nФамилия:{Surname}\nОтчество:{DadName}\nНомер Телефона:{Number}\nСтрана проживания:{Country}\nДата Рождения:{DateBirth.ToString("dd.MM.yyyy")}\nОрганизация:{Organization}\nДолжность:{Position}\nПрочие Заметки:{Other}";
        }
    }
}